
<?php
function getMenu ($menu, $vertical = true) {
    $style = '';
    if (!$vertical) {
        $style = ' style = "display: inline; margin-right:15px" ';
    }

    echo '<ul>';

    foreach ($menu as $link => $href) {
        echo "<li$style><a href =\"$href\">$link</a></li>";
    }

    echo '</ul>';
}

function getTable($cols=10, $rows=10, $color="red"){
    static $count = 0;
    echo '<table border="1">';
    for($tr=1; $tr<=$rows; $tr++){
        echo "<tr>";
        for($td=1; $td<=$cols; $td++){
            if($td==1 or $tr==1){
                echo "<th style='background-color:$color'>", $tr * $td, "</th>";
            }else{
                echo "<td>", $tr * $td, "</td>";
            }
        }
        echo "</tr>";
    }
    echo '</table>';
    $count++;
    $GLOBALS["count"] = $count;
}
?>



