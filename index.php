<?php
include_once "lib.inc.php";
include_once "topmenu.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <title>Шаблон сайта</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

<table width="100%" border="1">

    <tr>
        <td colspan="2" align="center">

            <?php
            include_once "top.inc.php";
            ?>
        </td>
    </tr>

    <tr>
        <td width="20%" valign="top">

            <?php
            include_once "menu.inc.php";
            ?>
        </td>
        <td>

            <?php
            $id = !empty($_GET['id']) ? strip_tags($_GET['id']) : null;
            switch ($id) {
                case 'page1.php':
                    include 'page1.php';
                    break;
                case 'page2.php':
                    include 'page2.php';
                    break;
                case 'page3.php':
                    include 'page3.php';
                    break;
                case 'table':
                    getTable();
                    break;
                case 'calculator.php':
                    include 'calculator.php';
                    break;
                default :
                    echo '<p>Привіт усім';
            }
            ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" align="center">

            <?php
            include_once "bottom.inc.php";
            ?>
        </td>
    </tr>
</table>
</body>
</html>