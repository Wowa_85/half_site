<table width="100%">
	<tr>
		<td align="center"><h3>Page 3</h3></td>
		<div> <br>GlobalLogic Ukraine is a talent team that collaborates with world technology leaders in creating innovative products and new quality standards of program engineering.

<p>We’re looking for enthusiastic and hard-working Manual QA Trainee to start IT career within our Lviv office team!

Project goals include testing of the video and audio content and other services available via digital media players and testing of media players firmware.

We offer:

<p>Interesting and challenging work in a large and dynamically developing company;
Exciting projects involving newest technologies;
Professional development opportunities;
Excellent compensation and benefits package, performance bonus program;
Modern and comfortable office facilities.</p> </br></div>
	</tr>
</table>